# 🐳 Image Docker Markdownlint

[![pipeline status](https://gitlab.com/typo-indus/docker/markdownlint/badges/main/pipeline.svg)](https://gitlab.com/typo-indus/docker/markdownlint/-/commits/main)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/48270638)](https://gitlab.com/typo-indus/docker/markdownlint/-/commits/main)
[![Latest Release](https://gitlab.com/typo-indus/docker/markdownlint/-/badges/release.svg)](https://gitlab.com/typo-indus/docker/markdownlint/-/releases)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/typo-indus/docker/markdownlint/-/blob/main/LICENSE)

Cette image Docker markdownlint vous permet de valider facilement les messages de commit selon des règles
conventionnelles.

## 🚀 Utilisation

1. Récupérez l'image Docker à partir du registre Docker :

```bash
docker pull registry.gitlab.com/typo-indus/docker/markdownlint:main
```

## 👥 Comment Contribuer

Pas de contribution pour le moment. Contactez-moi via les issues.

## 📄 License

Ce projet est sous licence MIT - voir le fichier LICENSE pour plus de détails.
