FROM node:22-alpine

# --------------------------------------------- Mets à jour les dépendances --------------------------------------------

RUN apk update && \
    apk upgrade && \
    rm -rf /var/cache/apk/*

# ----------------------------------------------- Installe markdownlint-cli --------------------------------------------

RUN npm update -g && npm install -g markdownlint-cli
